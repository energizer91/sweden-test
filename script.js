"use strict";

class Square {
    constructor(point, color) {
        this.x = point.x;
        this.y = point.y;
        this.color = color;
    }

    byRadius(radius) {
        this.radius = radius;
        return this;
    }

    moveTo(point) {
        this.x = point.x;
        this.y = point.y;
        return this;
    }

    byArea(area) {
        this.radius = Math.sqrt(area / Math.PI);
        return this;
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        ctx.lineWidth = 1;
        ctx.strokeStyle = this.color;
        ctx.stroke();
    }
}

class Point extends Square {
    constructor(x, y) {
        super({x, y}, 'red');
        return this.byRadius(5.5);
    }
}

class Parallelogram {
    constructor() {
        this.points = [];
        this.center = null;
        this.area = 0;
    }

    fromThreePoints(points) {
        if (points.length < 3) {
            return;
        }

        this.points = Array.from(points);

        if (this.points.length < 4) {
            this.points.push(new Point(this.points[2].x - (this.points[1].x - this.points[0].x), this.points[2].y - (this.points[1].y - this.points[0].y)));
        } else {
            this.points[3].moveTo({x: this.points[2].x - (this.points[1].x - this.points[0].x), y: this.points[2].y - (this.points[1].y - this.points[0].y)});
        }

        this.calculateCenter();
        this.calculateArea();

        return this;
    }

    calculateArea() {
        //calculate length of lines (points[0], points[1]) and (points[1], points[2])
        //calculate angle between them
        //multiply lines and sinus of an angle

        const x1 = this.points[0].x,
              y1 = this.points[0].y,
              x2 = this.points[1].x,
              y2 = this.points[1].y,
              x3 = this.points[2].x,
              y3 = this.points[2].y,
              vector1x = x1 - x2,
              vector1y = y1 - y2,
              vector2x = x2 - x3,
              vector2y = y2 - y3,
              line1 = Math.sqrt(Math.pow(vector1x, 2) + Math.pow(vector1y, 2)),
              line2 = Math.sqrt(Math.pow(vector2x, 2) + Math.pow(vector2y, 2)),
              angle = Math.acos((vector1x * vector2x + vector1y * vector2y) / (line1 * line2));

        this.area = line1 * line2 * Math.sin(angle);
        return this.area;
    }

    calculateCenter() {
        if (this.points.length < 4) {
            return;
        }

        const x1 = this.points[0].x,
              y1 = this.points[0].y,
              x2 = this.points[2].x,
              y2 = this.points[2].y,
              x3 = this.points[1].x,
              y3 = this.points[1].y,
              x4 = this.points[3].x,
              y4 = this.points[3].y,
              centerX = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)),
              centerY = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));

        this.center = new Point(centerX, centerY);
        return this.center;
    }

    drawHelperDiagonals(ctx) {
        if (this.points.length < 4) {
            return;
        }

        ctx.moveTo(this.points[0].x, this.points[0].y);
        ctx.lineTo(this.points[2].x, this.points[2].y);

        ctx.moveTo(this.points[1].x, this.points[1].y);
        ctx.lineTo(this.points[3].x, this.points[3].y);
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'blue';
        this.points.reduce((previous, next) => {
            if (previous.x && previous.y) {
                ctx.moveTo(previous.x, previous.y);
            }
            ctx.lineTo(next.x, next.y);
            return next;
        }, {});

        ctx.moveTo(this.points[3].x, this.points[3].y);
        ctx.lineTo(this.points[0].x, this.points[0].y);

        //this.drawHelperDiagonals(ctx);

        ctx.stroke();
    }
}

class Drawer {
    constructor(canvas) {
        this.points = [];
        this.canvas = canvas;
        this.showAbout = false;
        this.draggable = -1;
        this.parallelogram = null;
        this.square = null;
        this.canvas.addEventListener('mouseup', ({clientX, clientY}) => {
            if (this.draggable < 0) {
                return this.createPoint(clientX, clientY);
            } else {
                this.draggable = -1;
            }
        });
        this.canvas.addEventListener('mousedown', ({clientX, clientY}) => {
            const foundPoint = this.findPoint(clientX, clientY);
            if (foundPoint >= 0) {
                this.draggable = foundPoint;
            }
        });
        this.canvas.addEventListener('mousemove', ({clientX, clientY}) => {
            if (this.draggable >= 0) {
                this.points[this.draggable].moveTo({x: clientX, y: clientY});
            }
        });

        this.ctx = this.canvas.getContext('2d');
        this.loop = this.loop.bind(this);
    }

    createPoint(x, y) {
        if (this.points.length >= 3) {
            return;
        }

        return this.points.push(new Point(x, y)) - 1;
    }

    findPoint(x, y) {
        for (let i = 0; i < this.points.length; i++) {
            let dx = this.points[i].x - x,
                dy = this.points[i].y - y,
                distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

            if (distance < this.points[i].radius) {
                return i;
            }
        }

        return -1;
    }

    clearPoints() {
        this.points.length = 0;
    }

    updateStats(ctx) {
        let topPosition = 0;
        ctx.font = '12px serif';
        for (let i = 0; i < this.points.length; i++) {
            topPosition += 12;
            ctx.fillText('Point ' + (i + 1) + ' position: (' + this.points[i].x + ',' + this.points[i].y + ')', 5, topPosition);
        }
        if (this.parallelogram) {
            ctx.fillText('Parallelogram area: ' + Math.floor(this.parallelogram.area) + ' sq.px', 5, topPosition += 12);
        }
        if (this.square) {
            ctx.fillText('Square center: (' + Math.floor(this.square.x) + ',' + Math.floor(this.square.y) + ')', 5, topPosition += 12);
        }
        if (this.showAbout) {
            topPosition += 12;
            ctx.fillText('Three dots JS assignment.', 5, topPosition += 12);
            ctx.fillText('Author: Alexander Bareyko.', 5, topPosition += 12);
            ctx.fillText('---', 5, topPosition += 12);
            ctx.fillText('Just create 3 dots by clicking on white canvas.', 5, topPosition += 12);
            ctx.fillText('When it will be 3 points, a parallelogram and a square will appear.', 5, topPosition += 12);
            ctx.fillText('Square area equals to parallelogram area.', 5, topPosition += 12);
            ctx.fillText('---', 5, topPosition += 12);
            ctx.fillText('You can move dots to change result.', 5, topPosition += 12);
        }
    }

    toggleAbout() {
        this.showAbout = !this.showAbout;
    }

    loop() {
        const {width, height} = this.canvas;
        this.ctx.clearRect(0, 0, width, height);
        this.points.forEach((point) => {
            point.draw(this.ctx);
        });
        if (this.points.length == 3) {
            if (!this.parallelogram) {
                this.parallelogram = new Parallelogram();
            }
        } else {
            this.parallelogram = null;
            this.square = null;
        }

        if (this.parallelogram) {
            this.parallelogram.fromThreePoints(this.points).draw(this.ctx);

            if (!this.square) {
                this.square = new Square(this.parallelogram.center, 'yellow');
            }

            this.square.byArea(this.parallelogram.area).moveTo(this.parallelogram.center).draw(this.ctx);
        }

        this.updateStats(this.ctx);

        requestAnimationFrame(this.loop);
    }
}

const drawer = new Drawer(document.getElementById('mainCanvas'));

document.getElementById('clearAll').addEventListener('click', () => {
    drawer.clearPoints();
});

document.getElementById('about').addEventListener('click', () => {
    drawer.toggleAbout();
});

drawer.loop();